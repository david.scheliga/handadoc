# handadoc - Hand a Documentation
**handadoc** is about a simple documentation webserver, for mostly sphinx-build static
html documentations for small internal workgroups. While read-the-docs provides a
complete solution for sharing sphinx-based documentations, private repositories cannot
be used within the open source version.

This project enables to upload prebuild html documentations onto a portal, which will
publish the uploaded documentation as a static page. There is no ownership of
documentation items. Any logged in user can upload new versions of preexisting
documentations, as this tool is intended for small close working groups.

A command line tool for packaging and uploading is provided by
[handadoc-client](https://pypi.org/project/handadoc-client/).

# Deployment

1. Clone git repository.
2. Define your desired port for the webpage within the nginx-service setup within
   the docker-compose.yml file.
3. Start the webserver using docker-compose.
4. Initialize the server; create the database table and a superuser of your choice.

```` shell
docker-compose exec web python manage.py migrate
docker-compose exec web python manage.py createsuperuser
````
